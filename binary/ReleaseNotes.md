# Hand Eye Calibration Application Release Notes

### HandinEyeCalibration-v1.0, 2019-12-11
1. 集成了软件运行所需的所有依赖库文件，不需要用户安装QT，RobWork、OpenCV即可运行测试。
2. 在Windows 7 64bit和Windows 10 64bit中测试通过。

#### 操作说明
1. 解压“unzip-this-to-current-folder.zip”中内容至“HandinEyeCalibration-v1.0”文件夹下。
2. 双击“RobWorkStudio.exe”，打开仿真界面，然后通过界面菜单[File]-->[Open...]打开“workcell_scene\arvp\arvp.handeye.wc.xml”场景文件。
3. 按照项目说明文件ReadMe.md中“标定图像和机器人位姿采集”步骤进行标定样本采集，获得多组图片和对应的机器人关节角度（myfile.txt）。
4. 如果想跳过样本采集步骤，这里提供了测试样本放在“test-data-01.zip”中，解压到“HandinEyeCalibration-v1.0”文件夹中。
5. 为了方便查看测试结果，可通过cmd或git bash打开“CalibrationCalculator.exe”,直接得到标定结果。